Инструкция:

0) composer install   (Для установки Laravelium)
1) php artisan key:generate
2) в mysql создать БД interview
3) указать свои данные для подключения в .env
4) запустить php artisan migrate
5) php artisan db:seed (Генерация фейковых постов/статей,чтобы было что в сайтмапе показать)
6) php artisan serve
7) перейти по адресу http://127.0.0.1:8000/sitemap