 <?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   <url>
	<loc>http://localhost:8000/</loc>
	<lastmod>2021-04-14T17:15:17+01:00</lastmod>
	<priority>1.0</priority>
</url>
    @foreach ($posts as $p)
        <url>
            <loc>http://localhost:8000/posts/{{ $p->id }}</loc>
            <lastmod>{{ \Carbon\Carbon::parse($p->updated_at)->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>1</priority>
        </url>
    @endforeach

</urlset> 