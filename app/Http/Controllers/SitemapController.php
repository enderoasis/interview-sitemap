<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class SitemapController extends Controller
{

  
   public function index() {
       
	   return response()->view('sitemap.index')->header('Content-Type', 'text/xml');
 
 }

   public function posts() {

       $posts = Post::latest()->get();
       return response()->view('sitemap.post', [
           'posts' => $posts,
       ])->header('Content-Type', 'text/xml');
   }

}
